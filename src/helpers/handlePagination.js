import { ref, computed } from 'vue'
import store from '@/store'

export default function handlePagination () {
  const page = ref(1)

  const data = computed(() => {
    return store.getters.filteredNewsFeedDataGetter.map((item, index) => {
      item.index = index
      return item
    })
  })

  const perPage = 6

  const paginatedDataEntries = computed(() => {
    checkPageAvailability(page.value)
    return store.getters.filteredNewsFeedDataGetter.slice(
      (page.value - 1) * perPage,
      page.value * perPage
    )
  })

  function checkPageAvailability (pageValue) {
    if (
      store.getters.filteredNewsFeedDataGetter.length <
      (pageValue - 1) * perPage + 1
    ) {
      page.value = 1
    }
  }

  const nextPage = () => {
    if (
      page.value !==
      Math.ceil(store.getters.filteredNewsFeedDataGetter.length / perPage)
    ) {
      page.value += 1
    }
  }

  const backPage = () => {
    if (page.value !== 1) {
      page.value -= 1
    }
  }

  const goToPage = (numPage) => {
    page.value = numPage
  }

  return {
    data,
    paginatedDataEntries,
    perPage,
    page,
    nextPage,
    backPage,
    goToPage
  }
}
