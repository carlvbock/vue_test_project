const sortOrders = [
  { bezeichnung: 'Ascending', value: 'asc' },
  { bezeichnung: 'Descending', value: 'desc' }
]

const sortValues = [
  { bezeichnung: 'Author', value: 'author' },
  { bezeichnung: 'Titel', value: 'title' },
  { bezeichnung: 'Body', value: 'body' }
]

export { sortOrders, sortValues }
