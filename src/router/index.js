import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import NewsFeed from '@/views/NewsFeed'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/newsFeed',
    name: 'NewsFeed',
    component: NewsFeed
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
