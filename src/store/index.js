import Vuex from 'vuex'
import newsFeed from '@/store/modules/news-feed-module'

export const initialStoreModules = {
  newsFeed
}

export default new Vuex.Store({
  modules: initialStoreModules
})
