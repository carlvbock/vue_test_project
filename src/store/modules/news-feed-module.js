import uniqid from 'uniqid'
import _ from 'lodash'

const initData = [
  { id: '1', author: 'John', body: '1', title: 'a' },
  { id: '2', author: 'Jack', body: '2', title: 'b' },
  { id: '3', author: 'Jo', body: '3', title: 'c' },
  { id: '4', author: 'Jason', body: '4', title: 'd' },
  { id: '5', author: 'James', body: '5', title: 'e' },
  { id: '6', author: 'Jackson', body: '6', title: 'f' },
  { id: '7', author: 'Joseph', body: '7', title: 'g' },
  { id: '8', author: 'Jason', body: '8', title: 'h' },
  { id: '9', author: 'Johnson', body: '9', title: 'i' },
  { id: '10', author: 'Joshua', body: '10', title: 'j' },
  { id: '11', author: 'Jessica', body: '11', title: 'k' },
  { id: '12', author: 'Jake', body: '12', title: 'l' },
  { id: '13', author: 'Jamie', body: '13', title: 'm' },
  { id: '14', author: 'Jayden', body: '14', title: 'n' },
  { id: '15', author: 'Julia', body: '15', title: 'o' },
  { id: '16', author: 'Jasmine', body: '16', title: 'p' }
]

const defaultState = {
  searchBarFilter: {
    sortOrder: { bezeichnung: 'Ascending', value: 'asc' },
    sortValue: { bezeichnung: 'Author', value: 'author' },
    text: ''
  },
  newsFeedDataState: []
}

const initialiseSate = {}

const state = Object.assign(initialiseSate, defaultState)

const getters = {
  searchBarFilterGetter: (state) => state.searchBarFilter,
  filteredNewsFeedDataGetter: (state) => {
    return _.orderBy(
      state.searchBarFilter.text !== ''
        ? state.newsFeedDataState.filter(
          (newsFeedDataEntry) =>
            newsFeedDataEntry.author
              .toLowerCase()
              .includes(state.searchBarFilter.text.toLowerCase()) ||
              newsFeedDataEntry.body
                .toLowerCase()
                .includes(state.searchBarFilter.text.toLowerCase()) ||
              newsFeedDataEntry.title
                .toLowerCase()
                .includes(state.searchBarFilter.text.toLowerCase())
        )
        : state.newsFeedDataState,
      [state.searchBarFilter.sortValue.value],
      [state.searchBarFilter.sortOrder.value]
    )
  }
}

const actions = {
  async loadInitDataAction ({ commit }) {
    commit('setNewsFeedDataMutation', initData)
  },
  async createOrUpdateNewNewsFeedDataEntryAction ({ commit }, newOrUpdatedNewsFeedDataEntry) {
    commit('createOrUpdateNewsFeedDataEntryMutation', newOrUpdatedNewsFeedDataEntry)
  },
  async deleteNewsFeedDataEntryAction ({ commit }, newOrUpdatedNewsFeedDataEntry) {
    commit('deleteNewsFeedDataEntryMutation', newOrUpdatedNewsFeedDataEntry)
  }
}

const mutations = {
  createOrUpdateNewsFeedDataEntryMutation: (
    state,
    newOrUpdatedNewsFeedDataEntry
  ) => {
    const index = state.newsFeedDataState.findIndex(
      (newsFeedDataEntry) =>
        newsFeedDataEntry.id === newOrUpdatedNewsFeedDataEntry.id
    )
    if (index !== -1) {
      state.newsFeedDataState.splice(index, 1, newOrUpdatedNewsFeedDataEntry)
    } else {
      newOrUpdatedNewsFeedDataEntry.id = uniqid()
      state.newsFeedDataState = [
        ...state.newsFeedDataState,
        newOrUpdatedNewsFeedDataEntry
      ]
    }
  },
  deleteNewsFeedDataEntryMutation: (state, id) =>
    (state.newsFeedDataState = state.newsFeedDataState.filter(
      (newsFeedDataEntry) => newsFeedDataEntry.id !== id
    )),
  setNewsFeedDataMutation: (state, newsFeedDataState) => {
    state.newsFeedDataState = newsFeedDataState
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
